# Controller Thingy

It's a controller thingy, can I get a clap for such an original title?

## Setup
+ Clone this repo (`git clone https://gitlab.com/blankX/controller-thingy.git`)
+ cd into the repo's directory (`cd controller-thingy`)
    + Install the requirement (`pip3 install -r requirement.txt`)
    + Copy `sample.config.py` to `config.py` (`cp sample.config.py config.py`)
    + Edit `config.py` with your text editor

## Start
+ `python3 ub.py &`
