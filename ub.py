from telethon import TelegramClient, events, functions, types
from telethon.events import StopPropagation
from config import *

perms=god + trust + show + less_show
allowed=god + trust
god_world=False

import logging,datetime,subprocess
logging.basicConfig(level=logging.ERROR)

logging.debug(str(api_id) + ' ' + api_hash + ' ' + str(god))

ubc=TelegramClient('ubs', api_id, api_hash)

@ubc.on(events.NewMessage(incoming=True, from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, from_users=perms))
async def shall_god_be_the_one(e):
	print('Got ' + str(e.from_id))
	print('Checking if god')
	if not e.from_id in god:
		print('Not god')
		print('Checking god_world')
		try:
			global god_world
		except:
			print('Failed to global god_world')
		if god_world == True:
			print('god_world is True')
			raise StopPropagation
		else:
			print('god_world isn\'t True')
	else:
		print('Is god')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.godonly (Tru|Fals)e$', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.godonly (Tru|Fals)e$', from_users=perms))
async def yeye(e):
	try:
		global god_world
	except:
		print('Failed to global god_world')
	if e.pattern_match.group(1) == 'Fals':
		if e.from_id in god:
			r=await e.reply('`Yes, god.`')
			god_world=False
			await r.edit('`It is done.`')
		else:
			await e.reply('`No, sir`')
	else:
		r=await e.reply('`Yes, sir.`')
		god_world=True
		await r.edit('`It is done.`')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.stuff$', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.stuff$', from_users=perms))
async def stuff(e):
	await e.reply('''`Stuff:
.chat [message]
.ex_chat [user] [message]
.admin [user]
.demote [user]
.exec [cmd]
.godonly [True|False]
.stuff
.ppl
.stop`''')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.demote (.+)', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.demote (.+)', from_users=perms))
async def maekmidemot(e):
	if e.from_id in allowed:
		r=await e.reply('`Yes, sir.`')
		await ubc(functions.channels.EditAdminRequest(channel=e.chat_id, from_id=e.pattern_match.group(1), admin_rights=types.ChannelAdminRights(change_info=False,delete_messages=False,ban_users=False,invite_users=False,invite_link=False,pin_messages=False,add_admins=False,manage_call=False)))
		await r.edit('`It is done.`')
	else:
		await e.reply('`No, sir.`')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.admin (.+)', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.admin (.+)', from_users=perms))
async def maekmiadmen(e):
	if e.from_id in allowed:
		r=await e.reply('`Yes, sir.`')
		await ubc(functions.channels.EditAdminRequest(channel=e.chat_id, from_id=e.pattern_match.group(1), admin_rights=types.ChannelAdminRights(change_info=True,delete_messages=True,ban_users=True,invite_users=True,invite_link=True,pin_messages=True,add_admins=True,manage_call=False)))
		await r.edit('`It is done.`')
	else:
		await e.reply('`No, sir.`')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.ppl$', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.ppl$', from_users=perms))
async def guds(e):
	await e.reply('**God(s): **`' + str(god) + '`\n**Trusted: **`' + str(trust) + '`\n**For show: **`' + str(show) + '`\n**For show, but with less perms: **`' + str(less_show) + '`')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.ex_chat (.+?) (.+)', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.ex_chat (.+?) (.+)', from_users=perms))
async def ex_botc(e):
	idisnum=True
	cid=e.pattern_match.group(1)
	try:
		cid=int(cid)
	except:
		idisnum=False
	if e.from_id in allowed:
		await ubc.send_message(cid, e.pattern_match.group(2))
	else:
		if not e.from_id in less_show:
			sayity=e.pattern_match.group(2) + '\nSent by '
			if len(sayity + str(e.from_id)) < 4096:
				await ubc.send_message(cid, sayity + '[' + str(e.from_id) + '](tg://user?id=' + str(e.from_id) + ')')

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.chat (.+)', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.chat (.+)', from_users=perms))
async def botc(e):
	dore=False
	if e.reply_to_msg_id != None:
		dore=True
	if e.from_id in allowed:
		if dore == False:
			await ubc.send_message(e.chat_id, e.pattern_match.group(1))
		else:
			await ubc.send_message(e.chat_id, e.pattern_match.group(1), reply_to=e.reply_to_msg_id)
	else:
		if not e.from_id in less_show:
			sayity=e.pattern_match.group(1) + '\nSent by '
			if len(sayity + str(e.from_id)) < 4096:
				sayit=sayity + '[' + str(e.from_id) + '](tg://user?id=' + str(e.from_id) + ')'
				if dore == False:
					await ubc.send_message(e.chat_id, sayit)
				else:
					await ubc.send_message(e.chat_id, sayit, reply_to=e.reply_to_msg_id)

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.stop$', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.stop$', from_users=perms))
async def bots(e):
	if e.from_id in allowed:
		await e.reply('`Stopping`')
		await ubc.disconnect()
	else:
		await e.reply('`Not sotpping`')
#		Typo is intentional

@ubc.on(events.NewMessage(incoming=True, pattern='(?i)^.exec (.+)', from_users=perms))
@ubc.on(events.MessageEdited(incoming=True, pattern='(?i)^.exec (.+)', from_users=perms))
async def god_term(e):
	if e.from_id in god:
		cmd_res='```' + subprocess.run(e.pattern_match.group(1).split(' '), stdout=subprocess.PIPE).stdout.decode() + '```'
		if len(cmd_res) > 4096:
			f=open('cmd_result.txt', 'w+')
			f.write(cmd_res)
			f.close()
			await ubc.send_file(e.chat_id,'cmd_result.txt', reply_to=e.id, caption='`Done`')
			subprocess.run(['rm', 'cmd_result.txt'])
		else:
			await e.reply(cmd_res)
	else:
		p=e.raw_text.split(' ')
		a=["c#", "csharp", "vb.net", "vb", "visual_basic_dotnet", "f#", "fsharp", "java", "python2", "py2", "c_gcc", "gcc", "c", "cplusplus_gcc", "cplusplus", "g++", "c++", "cpp_gcc", "cpp", "php", "pascal", "pas", "fpc", "objective_c", "objc", "haskell", "ruby", "perl", "lua", "nasm", "asm", "sql_server", "v8", "common_lisp", "clisp", "lisp", "prolog", "golang", "go", "scala", "scheme", "node", "javascript", "js", "python3", "py3", "python", "c_clang", "clang", "cplusplus_clang", "cpp_clang", "clangplusplus", "clang++", "visual_cplusplus", "visual_cpp", "vc++", "msvc", "visual_c", "d", "r", "tcl", "mysql", "postgresql", "oracle", "swift", "bash", "ada", "erlang", "elixir", "ocaml", "kotlin", "brainfuck", "fortran"]
		if not a in p[1]:
			await e.reply('`No.`')

ubc.start()
ubc.run_until_disconnected()
